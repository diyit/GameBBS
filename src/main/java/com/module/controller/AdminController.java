package com.module.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.module.mapper.AdminMapper;
import com.module.mapper.InformMapper;
import com.module.mapper.UserinfoMapper;
import com.module.mapper.WenjuanMapper;
import com.module.mapper.WenjuanjieguoMapper;
import com.module.pojo.Admin;
import com.module.pojo.Inform;
import com.module.pojo.Userinfo;
import com.module.pojo.Wenjuan;
import com.module.pojo.Wenjuanjieguo;
import com.module.util.ResultUtil;

/**
 * 页面请求控制  帖子管理
 */
@Controller
public class AdminController {
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private UserinfoMapper userinfoMapper;
    @Autowired
    private WenjuanMapper wenjuanMapper;
    @Autowired
    private WenjuanjieguoMapper wenjuanjieguoMapper;
    
    @Autowired
    private InformMapper informMapper;
    
    


    /**
     * 跳转到列表页面
     *
     * @return
     */
    @RequestMapping("manage/login")
    public String articleList() {
        return "manage/login";
    }

    
    @ResponseBody
    @RequestMapping("manage/loginSubmit")
    public ResultUtil loginSubmit(String username, String password, String vcode, HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (StringUtils.isBlank(vcode)) {
            return ResultUtil.error("验证码不能为空");
        }
        String randCode = session.getAttribute("randCode").toString();
        if (!randCode.equals(vcode)) {
            return ResultUtil.error("验证码不正确");
        }
        if (StringUtils.isBlank(username)) {
            return ResultUtil.error("用户名不能为空");
        }
        if (StringUtils.isBlank(password)) {
            return ResultUtil.error("登录密码不能为空");
        }
        Map map = new HashMap();
        map.put("name", username);
        List<Admin> beanList = adminMapper.selectAll(map);
        if (beanList.size() > 0) {
        	Admin userinfo = beanList.get(0);
            if (userinfo.getAdminpassword().equals(password)) {
                session.setAttribute("loginUserinfo", userinfo);
                session.setAttribute("loginAdmin", userinfo);
                
                session.setAttribute("loginUserinfoID", userinfo.getId());
                return ResultUtil.ok("登录成功");
            } else {
                return ResultUtil.error("用户名或者密码错误");
            }
        } else {
            return ResultUtil.error("用户名或者密码错误");
        }

    }


    @RequestMapping("manage/index")
    public String index() {
        return "manage/index";
    }
    /*
         * 公告管理
     */
    @RequestMapping("manage/informList")
    public String informList() {
        return "manage/inform/informList";
    }
    @RequestMapping("manage/addInform")
    public String addInform() {
        return "manage/inform/saveInform";
    }
    
    
    @RequestMapping("manage/saveInform")
    @ResponseBody
    public ResultUtil saveInform(Inform inform, HttpSession session) {
        Date nowTime = new Date();
        inform.setCreatetime(nowTime);
        try {
        	informMapper.insertInform(inform);
            return ResultUtil.ok("添加管理员成功");
        } catch (Exception e) {
            return ResultUtil.error("添加管理员出错,稍后再试！");
        }
    }
    
    
    
    @RequestMapping("manage/updateInform")
    @ResponseBody
    public ResultUtil updateInform(Inform inform, HttpSession session) {
        Date nowTime = new Date();
        inform.setCreatetime(nowTime);
        try {
        	informMapper.updateInform(inform);
            return ResultUtil.ok("修改成功");
        } catch (Exception e) {
            return ResultUtil.error("修改出错,稍后再试！");
        }
    }
    
    @RequestMapping("manage/deleteInform")
    @ResponseBody
    public ResultUtil deleteInform(Integer id) {
        try {
        	informMapper.deleteInformById(id);
            return ResultUtil.ok("删除成功");
        } catch (Exception e) {
            return ResultUtil.error("删除出错,稍后再试！");
        }
    }
    
    @RequestMapping("manage/deletesInform")
    @ResponseBody
    public ResultUtil deleteInform(String idsStr) {
        try {
            if (!StringUtils.isBlank(idsStr)) {
                String[] ids = idsStr.split(",");
                for (String id : ids) {
                	informMapper.deleteInformById(Integer.parseInt(id));
                }
            }
            return ResultUtil.ok("批量删除成功");
        } catch (Exception e) {
            return ResultUtil.error("删除出错,稍后再试！");
        }
    }

    /**
     * 
     * @return
     */
    @RequestMapping("manage/adminList")
    public String adminList() {
        return "manage/admin/adminList";
    }
    
    @RequestMapping("manage/addAdmin")
    public String addAdmin() {
        return "manage/admin/saveAdmin";
    }
    @RequestMapping("manage/saveAdmin")
    @ResponseBody
    public ResultUtil saveArticle(Admin admin, HttpSession session) {
        Date nowTime = new Date();
        admin.setCreatetime(nowTime);
        try {
            adminMapper.insertAdmin(admin);
            return ResultUtil.ok("添加管理员成功");
        } catch (Exception e) {
            return ResultUtil.error("添加管理员出错,稍后再试！");
        }
    }
    
    
    
    @RequestMapping("manage/updateAdmin")
    @ResponseBody
    public ResultUtil updateAdmin(Admin admin, HttpSession session) {
        Date nowTime = new Date();
        admin.setCreatetime(nowTime);
        try {
        	adminMapper.updateAdmin(admin);
            return ResultUtil.ok("修改成功");
        } catch (Exception e) {
            return ResultUtil.error("修改出错,稍后再试！");
        }
    }
    
    @RequestMapping("manage/deleteAdmin")
    @ResponseBody
    public ResultUtil deleteAdmin(Integer id) {
        try {
        	adminMapper.deleteAdminById(id);
            return ResultUtil.ok("删除成功");
        } catch (Exception e) {
            return ResultUtil.error("删除出错,稍后再试！");
        }
    }
    
    @RequestMapping("manage/deletesAdmin")
    @ResponseBody
    public ResultUtil deletesAdmin(String idsStr) {
        try {
            if (!StringUtils.isBlank(idsStr)) {
                String[] ids = idsStr.split(",");
                for (String id : ids) {
                    adminMapper.deleteAdminById(Integer.parseInt(id));
                }
            }
            return ResultUtil.ok("批量删除成功");
        } catch (Exception e) {
            return ResultUtil.error("删除出错,稍后再试！");
        }
    }

    
    
    
    /*
     * 数据分析
     */
    @RequestMapping("manage/countDateShow")
    public String countDateShow() {
        return "manage/article/countDateShow";
    }
    /*
         * 问卷结果
     * @return
     */
    @RequestMapping("manage/wenjuanjieguoList")
    public String wenjuanjieguoList() {
        return "manage/wenjuanjieguo/wenjuanjieguoList";
    }
    @RequestMapping("manage/addWenjuanjieguo")
    public String addWenjuanjieguo() {
        return "manage/wenjuanjieguo/saveWenjuanjieguo";
    }
    @RequestMapping("manage/saveWenjuanjieguo")
    @ResponseBody
    public ResultUtil saveWenjuanjieguo(Wenjuanjieguo wenjuanjieguo, HttpSession session) {
        Date nowTime = new Date();
        wenjuanjieguo.setCreatetime(nowTime);
        try {
        	wenjuanjieguoMapper.insertWenjuanjieguo(wenjuanjieguo);
            return ResultUtil.ok("添加成功");
        } catch (Exception e) {
            return ResultUtil.error("添加出错,稍后再试！");
        }
    }
    
    
    @RequestMapping("manage/updateWenjuanjieguo")
    @ResponseBody
    public ResultUtil updateWenjuan(Wenjuanjieguo wenjuanjieguo, HttpSession session) {
        Date nowTime = new Date();
        wenjuanjieguo.setCreatetime(nowTime);
        try {
        	wenjuanjieguoMapper.updateWenjuanjieguo(wenjuanjieguo);
            return ResultUtil.ok("修改成功");
        } catch (Exception e) {
            return ResultUtil.error("修改出错,稍后再试！");
        }
    }
    
    @RequestMapping("manage/deleteWenjuanjieguo")
    @ResponseBody
    public ResultUtil deleteWenjuanjieguo(Integer id) {
        try {
        	wenjuanjieguoMapper.deleteWenjuanjieguoById(id);
            return ResultUtil.ok("删除成功");
        } catch (Exception e) {
            return ResultUtil.error("删除出错,稍后再试！");
        }
    }
    
    @RequestMapping("manage/deletesWenjuanjieguo")
    @ResponseBody
    public ResultUtil deletesWenjuanjieguo(String idsStr) {
        try {
            if (!StringUtils.isBlank(idsStr)) {
                String[] ids = idsStr.split(",");
                for (String id : ids) {
                	wenjuanjieguoMapper.deleteWenjuanjieguoById(Integer.parseInt(id));
                }
            }
            return ResultUtil.ok("批量删除成功");
        } catch (Exception e) {
            return ResultUtil.error("删除出错,稍后再试！");
        }
    }

    
    
    
    /*
     * 问卷
     */
    @RequestMapping("manage/wenjuanList")
    public String wenjuanList() {
        return "manage/wenjuan/wenjuanList";
    }
    @RequestMapping("manage/addWenjuan")
    public String addWenjuan() {
        return "manage/wenjuan/saveWenjuan";
    }
    
    @RequestMapping("manage/saveWenjuan")
    @ResponseBody
    public ResultUtil saveWenjuan(Wenjuan wenjuan, HttpSession session) {
        Date nowTime = new Date();
        wenjuan.setCreatetime(nowTime);
        try {
        	wenjuanMapper.insertWenjuan(wenjuan);
            return ResultUtil.ok("添加成功");
        } catch (Exception e) {
            return ResultUtil.error("添加出错,稍后再试！");
        }
    }
    
    @RequestMapping("manage/updateWenjuan")
    @ResponseBody
    public ResultUtil updateWenjuan(Wenjuan wenjuan, HttpSession session) {
        Date nowTime = new Date();
        wenjuan.setCreatetime(nowTime);
        try {
        	wenjuanMapper.updateWenjuan(wenjuan);
            return ResultUtil.ok("修改成功");
        } catch (Exception e) {
            return ResultUtil.error("修改出错,稍后再试！");
        }
    }
    
    
    
    @RequestMapping("manage/deleteWenjuan")
    @ResponseBody
    public ResultUtil deleteWenjuan(Integer id) {
        try {
        	wenjuanMapper.deleteWenjuanById(id);
            return ResultUtil.ok("删除成功");
        } catch (Exception e) {
            return ResultUtil.error("删除出错,稍后再试！");
        }
    }
    
    
    @RequestMapping("manage/deletesWenjuan")
    @ResponseBody
    public ResultUtil deletesWenjuan(String idsStr) {
        try {
            if (!StringUtils.isBlank(idsStr)) {
                String[] ids = idsStr.split(",");
                for (String id : ids) {
                	wenjuanMapper.deleteWenjuanById(Integer.parseInt(id));
                }
            }
            return ResultUtil.ok("批量删除成功");
        } catch (Exception e) {
            return ResultUtil.error("删除出错,稍后再试！");
        }
    }
    
    /*
         * 用户管理 
     * @return
     */
    @RequestMapping("manage/userinfoList")
    public String userinfoList() {
        return "manage/userinfo/userinfoList";
    }
    /**
     * 添加用户
     * @return
     */
    @RequestMapping("manage/addUserinfo")
    public String addUserinfo() {
        return "manage/userinfo/saveUserinfo";
    }
    
    @RequestMapping("manage/saveUserinfo")
    @ResponseBody
    public ResultUtil saveUserinfo(Userinfo userinfo, HttpSession session) {
        Date nowTime = new Date();
        userinfo.setCreatetime(nowTime);
        try {
        	userinfoMapper.insertUserinfo(userinfo);
            return ResultUtil.ok("添加成功");
        } catch (Exception e) {
            return ResultUtil.error("添加出错,稍后再试！");
        }
    }
    
    @RequestMapping("manage/updateUserinfo")
    @ResponseBody
    public ResultUtil updateArticle(Userinfo userinfo, HttpSession session) {
        Date nowTime = new Date();
        userinfo.setCreatetime(nowTime);
        try {
        	userinfoMapper.updateUserinfo(userinfo);
            return ResultUtil.ok("修改成功");
        } catch (Exception e) {
            return ResultUtil.error("修改出错,稍后再试！");
        }
    }
    
    @RequestMapping("manage/deleteUserinfo")
    @ResponseBody
    public ResultUtil deleteUserinfo(Integer id) {
        try {
        	userinfoMapper.deleteUserinfoById(id);
            return ResultUtil.ok("删除成功");
        } catch (Exception e) {
            return ResultUtil.error("删除出错,稍后再试！");
        }
    }
    
    @RequestMapping("manage/deletesUserinfo")
    @ResponseBody
    public ResultUtil deletesUserinfo(String idsStr) {
        try {
            if (!StringUtils.isBlank(idsStr)) {
                String[] ids = idsStr.split(",");
                for (String id : ids) {
                	userinfoMapper.deleteUserinfoById(Integer.parseInt(id));
                }
            }
            return ResultUtil.ok("批量删除成功");
        } catch (Exception e) {
            return ResultUtil.error("删除出错,稍后再试！");
        }
    }
    
  
   
    @RequestMapping("manage/personalData")
    public String personalData(HttpSession session,Model model) {
    	model.addAttribute("admin",session.getAttribute("loginAdmin"));
        return "manage/personalData";
    }
    
    
    @RequestMapping("manage/main")
    public String main() {
        return "manage/main";
    }

}
