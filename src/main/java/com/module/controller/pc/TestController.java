package com.module.controller.pc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 前端控制器
 */
@Controller
public class TestController  {


    /**
     * 测试页面
     *
     * @param model
     * @return
     */
    @RequestMapping("test/testPage")
    public String testPage(Model model) {
        return "pc/testPage";
    }

    @RequestMapping("test/testTable")
    public String testTable(Model model) {
        return "pc/testTable";
    }

}
